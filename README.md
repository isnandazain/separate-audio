# Separate PCAP File to Audio

# Install Packages
pip install -r requirements.txt

# Parameter
- pcap_filename (nama file pcap)
- output_filename (pattern filename audio output)
- filter_type (default: rtp)
- directory_output (directory untuk hasil separate)