import pyshark
import time

from pathlib import Path
import os
import subprocess
import shlex

from concurrent.futures import ThreadPoolExecutor

def _os_execute(command):
    result, err = subprocess.Popen(
        shlex.split(command),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE).communicate()
    return result, err


def separate_audio(pcap_filename="test_180s.pcap", output_filename="output_audio", directory_output=None, filter_type="rtp"):
    current_dir = Path(os.path.dirname(os.path.abspath(__file__)))
    executor = ThreadPoolExecutor(max_workers=4)
    cap = pyshark.FileCapture(pcap_filename, display_filter=filter_type)

    # create directory if not exist and not None
    if directory_output:
        current_dir = current_dir / directory_output
        if not current_dir.is_dir():
            current_dir.mkdir()

    # process
    rtp_list = [""]
    index_loop = 0
    for index, data in enumerate(cap):
        try:
            rtp = data[3]
            if rtp.payload:
                index_loop += 1

                if index_loop == 500:
                    convert(index, "audio_raw", output_filename, directory_output, rtp_list)

                    index_loop = 0
                    rtp_list = [""]
                else:
                    rtp_split = rtp.payload.split(":")
                    packet = " ".join(rtp_split) + " "
                    rtp_list[0] = rtp_list[0] + packet

        except:
            pass
        

def convert(index, audio_filename, output_filename, directory_output, packet):
    # generate filename
    audio_filename = "%s_%s%s" % (audio_filename, index, ".raw")
    output_filename = "%s_%s%s" % (output_filename, index, ".wav")

    if directory_output:
        audio_filename = directory_output + "/" + audio_filename
        output_filename = directory_output + "/" + output_filename

    audio = bytearray.fromhex(packet[0][:-1])

    raw_audio = open(audio_filename, 'wb')
    raw_audio.write(audio)

    _os_execute("sox --type raw --rate 44100 -e u-law {} {}".format(audio_filename, output_filename))
    #_os_execute("ffmpeg -f mulaw -ar 44100 -i {} {}".format(audio_filename, output_filename))

    # delete raw file
    # os.remove(audio_filename)


separate_audio(pcap_filename="test_180s.pcap",
               output_filename="output_audio",
               filter_type="rtp",
               directory_output="result")